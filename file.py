
def task_1():
    # 1. Напишите программу для нахождения суммы цифр заданного трехзначного числа.

    while True:

        number = input('Введите трехзначное число... \n')
        if not number.isdigit() or len(number) != 3:
            print('Введено недопустимое число, повторите попытку:\n')
        else:
            break

    from functools import reduce
    result = reduce(lambda x, y: int(x) + int(y), number)
    print(f'Сумма цифр трехзначного числа {number} равна {result}\n')

def task_2():
    # 2. Вычислить периметр прямоугольника, длина и ширина вводятся с клавиатуры.

    while True:

        number_one = input('Введите длинну... \n')
        number_two = input('Введите ширину... \n')

        if not number_one.isdigit() or not number_two.isdigit():
            print('Введены недопустимые числа, повторите попытку:\n')
        else:
            break
    P = 2 * (int(number_one) + int(number_two))
    print(f'Периметр прямоугольника равен: {P} \n')
    
def task_3():
    # 3. Вычислить рациональным способом (за минимальное количество операций) следующие выражения:
    # а)  y=X^5        б)  y=X^6 в)         y=X^8

    while True:

        number = input('Введите число Х... \n')
        if not number.isdigit():
            print('Введено недопустимое число, повторите попытку:\n')
        else:
            break
    number = int(number)

    def fast_pow(num, n):
        """Алгоритм быстрого возведения в степень"""

        if n % 2 == 0:
            return (num ** 2) ** (n / 2)
        if n % 2 != 0:
            return num * fast_pow(num, n-1)
        return num
    pows = (fast_pow(number, 5), fast_pow(number, 6), fast_pow(number, 8))

    print(f'Результат возведения в степень: x^5={int(pows[0])}  x^6={int(pows[1])}  x^8={int(pows[2])}')

def task_4():
    from sys import exit
    print('Завершение работы')
    exit()

def menu_display():
    print('''
            Выберите один из пунктов меню:\n
            1. Вычислить сумму цифр трехзначного числа.\n
            2. Вычислить периметр прямоугольника.\n
            3. Возвести число в степень: 5, 6, 8.\n
            4. Закрыть программу
          '''
          )

def user_select():
    while True:
        num_selected = input('...')

        if not num_selected.isdigit() or num_selected not in ('1', '2', '3', '4'):
            print('Ошибка, такого пункта не существует, повторите команду')
        else:
            break

    num = int(num_selected)
    print(f'Выбран пункт №{num}')
    return num

def main():
    
    while True:
        menu_display()

        selected = user_select()
        task = 'task_' + str(selected)
        globals()[task]()

if '__main__' == __name__:
    main()
